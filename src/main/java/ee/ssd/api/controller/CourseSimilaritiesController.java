package ee.ssd.api.controller;

import ee.ssd.api.data.dto.response.similarity.SimilarityResponse;
import ee.ssd.api.data.dto.response.error.InternalErrorResponse;
import ee.ssd.api.data.entity.Similarity;
import ee.ssd.api.service.CourseSimilaritiesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Api(tags = "CourseSimilarities")
@RestController
@RequestMapping(value = "/courseSimilarities", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class CourseSimilaritiesController extends AbstractApiController {

    protected final Logger logger = LoggerFactory.getLogger(CourseSimilaritiesController.class);

    @Autowired
    private CourseSimilaritiesService courseSimilaritiesService;


    @RequestMapping(path = "similarities/{code}",method = RequestMethod.GET)
    @ApiResponses(value = {
            @ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Internal Error", response = InternalErrorResponse.class)
    })
    @ApiOperation(
            value = "Get similarities of related courses",
            tags = "CourseDistances",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public List<SimilarityResponse> findDistances(@PathVariable String code) throws IOException {
        List<Similarity> correlationList = courseSimilaritiesService.findAll(code);
        Collections.sort(correlationList, Comparator.comparing(Similarity::getSimilarity));
        return correlationList.stream().map(SimilarityResponse::convertFromEntity).collect(Collectors.toList());
    }
}
