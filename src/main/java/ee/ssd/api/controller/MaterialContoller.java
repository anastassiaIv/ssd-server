package ee.ssd.api.controller;

import ee.ssd.api.data.dto.request.material.MaterialListGetRequest;
import ee.ssd.api.data.dto.response.error.InternalErrorResponse;
import ee.ssd.api.data.dto.response.material.MaterialResponse;
import ee.ssd.api.data.entity.Material;
import ee.ssd.api.service.MaterialService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Api(tags = "Materials")
@RestController
@RequestMapping(value = "/materials", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class MaterialContoller  extends AbstractApiController {

    private static final Logger logger = LoggerFactory.getLogger(MaterialContoller.class);

    @Autowired
    MaterialService materialService;

    @RequestMapping(path = "/list",method = RequestMethod.GET)
    @ApiResponses(value = {
            @ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Internal Error", response = InternalErrorResponse.class)
    })
    @ApiOperation(
            value = "Get list of materials",
            tags = "Materias",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public List<MaterialResponse> materialsByCourseList(
            @RequestParam(defaultValue = "code") String code

    ) throws IOException {
        logger.info("materialsByCourseList: " + code);
        MaterialListGetRequest request = new MaterialListGetRequest(code);
        List<Material> materials = materialService.find(request);
        return materials.stream().map(MaterialResponse::convertFromEntity).collect(Collectors.toList());
    }
}
