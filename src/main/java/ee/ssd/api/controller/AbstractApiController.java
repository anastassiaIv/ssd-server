package ee.ssd.api.controller;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import ee.ssd.api.controller.exception.*;
import ee.ssd.api.controller.util.ExceptionInfoLogger;
import ee.ssd.api.data.dto.response.error.*;
import ee.ssd.api.exception.AccessRestrictedException;
import ee.ssd.api.service.CourseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class AbstractApiController {


    @Autowired
    CourseService courseService;

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    private final Map<String, String> enumErrors = new HashMap<>();

    @ExceptionHandler(ValidationException.class)
    @ResponseBody
    public ValidationErrorResponse handleValidationException(HttpServletResponse response, ValidationException e) {
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setStatus(e.getStatus());

        return new ValidationErrorResponse(e);
    }

    @ExceptionHandler(NotFoundException.class)
    @ResponseBody
    public NotFoundErrorResponse handleValidationException(HttpServletResponse response, NotFoundException e) {
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setStatus(e.getStatus());

        return new NotFoundErrorResponse(e);
    }

    @ExceptionHandler(ApiException.class)
    @ResponseBody
    public ErrorResponse handleApiException(HttpServletResponse response, ApiException e) {
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setStatus(e.getStatus());

        return new ErrorResponse(e.getCode());
    }

    @ExceptionHandler(AccessRestrictedException.class)
    @ResponseBody
    public ErrorResponse handleForbidden(HttpServletRequest request, HttpServletResponse response, AccessRestrictedException e) {
        ExceptionInfoLogger.printForbiddenError(request, e);

        response.setStatus(HttpServletResponse.SC_FORBIDDEN);

        return new ErrorResponse(ErrorCodeEnum.FORBIDDEN);
    }

    @ExceptionHandler(AccessDeniedException.class)
    @ResponseBody
    public ErrorResponse handleForbidden(HttpServletRequest request, HttpServletResponse response, AccessDeniedException e) {
        ExceptionInfoLogger.printForbiddenError(request, e);

        response.setStatus(HttpServletResponse.SC_FORBIDDEN);

        return new ErrorResponse(ErrorCodeEnum.FORBIDDEN);
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    @ResponseBody
    public ValidationErrorResponse handleMissingServletRequestParameterException(HttpServletResponse response, MissingServletRequestParameterException ex) {
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);

        return new ValidationErrorResponse(new ValidationErrorRow(ex.getParameterName(), ValidationErrorCodeEnum.REQUIRED));
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseBody
    public ValidationErrorResponse handleInvalidFormatException(HttpServletResponse response, HttpMessageNotReadableException ex) throws Throwable {

        if (ex.getMessage().contains("Required request body is missing")) {
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);

            new ValidationErrorResponse(ErrorCodeEnum.BODY_MISSING);
        }

        if (!(ex.getCause() instanceof InvalidFormatException)) throw ex;

        if (ex.getMessage().contains("java.math.BigDecimal from String")) {
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);

            String field = null;
            Matcher matcher = Pattern.compile("through reference chain: [a-zA-z\\.]*\"(.*)\"\\]").matcher(ex.getMessage());

            if (matcher.find()) {
                field = matcher.group(1);
            }

            return new ValidationErrorResponse(new ValidationErrorRow(field, ValidationErrorCodeEnum.INVALID_DATA_TYPE));
        }

        return checkForEnumError(response, ex);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    @ResponseBody
    public ValidationErrorResponse handleTypeMismatchException(HttpServletResponse response, MethodArgumentTypeMismatchException ex) throws Throwable {
        if (ex.getMessage().contains("java.util.Date")) {
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);

            return new ValidationErrorResponse(new ValidationErrorRow(ex.getName(), ValidationErrorCodeEnum.INVALID));
        }

        if (ex.getMessage().contains("java.lang.NumberFormatException")) {
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);

            return new ValidationErrorResponse(new ValidationErrorRow(ex.getName(), ValidationErrorCodeEnum.INVALID));
        }

        return checkForEnumError(response, ex);
    }

    @ExceptionHandler(IOException.class)
    @ResponseBody
    public void handleIOException(HttpServletResponse response, IOException ex) {
        logger.warn(ex.getMessage());
        response.setStatus(HttpServletResponse.SC_NO_CONTENT);
    }

    private ValidationErrorResponse checkForEnumError(HttpServletResponse response, Throwable ex) throws Throwable {
        for (Map.Entry<String, String> entry : enumErrors.entrySet()) {
            if (ex.getMessage().contains(entry.getKey())) {
                response.setContentType(MediaType.APPLICATION_JSON_VALUE);
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);

                return new ValidationErrorResponse(new ValidationErrorRow(entry.getValue(), ValidationErrorCodeEnum.INVALID));
            }
        }

        throw ex;
    }

    protected void checkErrors(BindingResult bindingResult) {
        checkErrors(bindingResult, new HashSet<>());
    }

    protected void checkErrors(Set<ValidationErrorRow> errors) {
        if (!errors.isEmpty()) throw new ValidationException(errors);
    }

    protected void checkErrors(BindingResult bindingResult, Set<ValidationErrorRow> errors) {
        if (errors.isEmpty() && !bindingResult.hasErrors()) return;

        ValidationException ex = new ValidationException(errors);

        for (ObjectError objectError : bindingResult.getAllErrors()) {
            ex.addRow(objectError.getObjectName(), ValidationErrorCodeEnum.INVALID_DATA_TYPE);
        }

        throw ex;
    }
}
