package ee.ssd.api.controller;

import ee.ssd.api.data.dto.response.course.CourseResponse;
import ee.ssd.api.data.dto.response.error.InternalErrorResponse;;
import ee.ssd.api.data.entity.Course;
import ee.ssd.api.service.CourseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.stream.Collectors;

@Api(tags = "Courses")
@RestController
@RequestMapping(value = "/courses", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class CoursesController extends AbstractApiController  {

    protected final Logger logger = LoggerFactory.getLogger(CoursesController.class);

    @Autowired
    private CourseService coursesService;


    @RequestMapping(path = "/all",method = RequestMethod.GET)
    @ApiResponses(value = {
            @ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Internal Error", response = InternalErrorResponse.class)
    })
    @ApiOperation(
            value = "Get list of all courses",
            tags = "Courses",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public List<CourseResponse> allCoursesList() {
        List<Course> coursesList = coursesService.findAll();
        return coursesList.stream().map(CourseResponse::convertFromEntity).collect(Collectors.toList());
    }
}
