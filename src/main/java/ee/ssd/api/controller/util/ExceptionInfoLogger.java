package ee.ssd.api.controller.util;

import ee.ssd.api.config.SecurityConfig;
import ee.ssd.api.exception.AccessRestrictedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;

import javax.servlet.http.HttpServletRequest;

public class ExceptionInfoLogger{
    private static final Logger logger = LoggerFactory.getLogger(ExceptionInfoLogger.class);
    private static final String serverErrorTemplate = "ID: '%s', request: '%s', token: '%s', API KEY: '%s', message: '%s'";
    private static final String forbiddenTemplate = "Access to resource forbidden! Request: '%s', token: '%s', API KEY: '%s', message: '%s'";

    public static void printServerError(HttpServletRequest request, Throwable e, String errorId) {
        // TODO fix this and add more info
        e.printStackTrace();
        logger.error(String.format(
                serverErrorTemplate,
                errorId,
                getPath(request),
                request.getHeader(SecurityConfig.TOKEN_HEADER_NAME),
                request.getHeader(SecurityConfig.API_KEY_HEADER_NAME),
                e.getMessage()
        ), e);
    }

    public static void printForbiddenError(HttpServletRequest request, AccessRestrictedException e) {
        printForbidden(request, e);
    }

    public static void printForbiddenError(HttpServletRequest request, AccessDeniedException e) {
        printForbidden(request, e);
    }

    private static void printForbidden(HttpServletRequest request, RuntimeException e) {
        // TODO fix this and add more info
        e.printStackTrace();
        logger.warn(String.format(
                forbiddenTemplate,
                getPath(request),
                request.getHeader(SecurityConfig.TOKEN_HEADER_NAME),
                request.getHeader(SecurityConfig.API_KEY_HEADER_NAME),
                e.getMessage()
        ));
    }

    private static String getPath(HttpServletRequest request) {
        String path = request.getMethod() + " " + request.getRequestURI();

        if (request.getQueryString() != null) path += "?" + request.getQueryString();

        return path;
    }
}
