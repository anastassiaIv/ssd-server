package ee.ssd.api.controller.exception;

public class NotFoundException extends ApiException {
    private final EntityEnum entity;
    private final String field;
    private final String id;

    public NotFoundException(EntityEnum entity, Long id) {
        this(entity, "id", id.toString());
    }

    public NotFoundException(EntityEnum entity, String field, String value) {
        super(ErrorCodeEnum.NOT_FOUND, 404);
        this.entity = entity;
        this.field = field;
        this.id = value;
    }

    public EntityEnum getEntity() {
        return entity;
    }

    public String getField() {
        return field;
    }

    public String getId() {
        return id;
    }
}
