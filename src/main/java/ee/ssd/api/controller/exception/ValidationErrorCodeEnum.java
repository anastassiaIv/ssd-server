package ee.ssd.api.controller.exception;

public enum ValidationErrorCodeEnum {
    DUPLICATE, REQUIRED, NOT_FOUND, NOT_ALLOWED, INVALID, INVALID_DATA_TYPE, EXPIRED, FORBIDDEN
}
