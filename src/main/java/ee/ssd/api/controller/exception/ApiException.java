package ee.ssd.api.controller.exception;

public class ApiException extends RuntimeException {

    private final ErrorCodeEnum code;
    private final int status;

    ApiException(ErrorCodeEnum code, int status) {
        this.code = code;
        this.status = status;
    }

    public static ApiException forbidden() {
        return new ApiException(ErrorCodeEnum.FORBIDDEN, 403);
    }

    public ErrorCodeEnum getCode() {
        return code;
    }

    public int getStatus() {
        return status;
    }
}
