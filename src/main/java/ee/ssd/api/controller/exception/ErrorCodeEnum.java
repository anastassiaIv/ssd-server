package ee.ssd.api.controller.exception;

public enum ErrorCodeEnum {
    VALIDATION_ERROR,       // 400
    BODY_MISSING,           // 400
    CANNOT_DELETE,          // 400
    CANNOT_ARCHIVE,         // 400
    CANNOT_ACTIVATE,        // 400
    CANNOT_EXPORT,          // 400
    UNAUTHORIZED,           // 401
    FORBIDDEN,              // 403
    UNABLE_TO_DELETE,          // 403
    NOT_FOUND,              // 404
    METHOD_NOT_ALLOWED,     // 405
    CONFLICT,               // 409 - resource already exists
    INTERNAL_ERROR,         // 500
}
