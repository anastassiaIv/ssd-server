package ee.ssd.api.controller.exception;

import javax.servlet.http.HttpServletResponse;
import java.util.HashSet;
import java.util.Set;

import ee.ssd.api.data.dto.response.error.ValidationErrorRow;


public class ValidationException extends ApiException {

    private final Set<ValidationErrorRow> rows = new HashSet<>();
    private final String message;

    public ValidationException(Set<ValidationErrorRow> rows) {
        super(ErrorCodeEnum.VALIDATION_ERROR, HttpServletResponse.SC_BAD_REQUEST);
        this.rows.addAll(rows);
        this.message = null;
    }

    public void addRow(String field, ValidationErrorCodeEnum code) {
        rows.add(new ValidationErrorRow(field, code));
    }


    public Set<ValidationErrorRow> getRows() {
        return rows;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
