package ee.ssd.api.service;

import ee.ssd.api.service.material.RecommendArticleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Component
public class SchedulingService {

    private static final Logger log = LoggerFactory.getLogger(SchedulingService.class);

    @Autowired
    private RunPythonCodeService runPythonCodeService;

    @Autowired
    private FileHandleService fileHandleService;

    @Autowired
    private RecommendArticleService recommendArticleService;

    @Value("${tokensLocation}")
    private String tokensLocation;

    @Value("${materialsLocation}")
    private String materialsLocation;

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Scheduled(cron="0 5 1 1 * ?") //every month 1 date at 5AM
    //@Scheduled(cron="0 0 0/1 * * ?") //evry hour
    // @Scheduled(cron="0 0/40 * * * ?") //evry 40 min
    private void reportCurrentTime() throws IOException {
        log.info("Starting scheduled task");
        long methodStart = System.currentTimeMillis();
        log.info("The time is now {}", dateFormat.format(new Date()));
        fileHandleService.clearDirecotry(tokensLocation);
        fileHandleService.clearDirecotry(materialsLocation);

        // cleaning database
        // runPythonCodeService.cleanDb();

        // cleaning raw data
        // runPythonCodeService.cleanRawData();

        // getting tokens for all courses
        runPythonCodeService.getTokensForAllCourses();

        // cleaning database
        // runPythonCodeService.cleanDb();

        // cleaning raw data
        // runPythonCodeService.cleanRawData();

        // parsing all tokens

        File folder = new File(tokensLocation);
        File[] listOfFiles = folder.listFiles();

        for (File file : listOfFiles) {
            if (file.isFile()) {
                log.info("File: " + file.getName().replace(".txt", ""));
                recommendArticleService.getRecommendedArticlesForCourse(file.getName().replace(".txt", ""));

            }
        }

        long methodEnd = System.currentTimeMillis();
        log.info("TIME: " +  TimeUnit.MILLISECONDS.toSeconds(methodEnd - methodStart) + " sek");
    }
}
