package ee.ssd.api.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

public abstract class AbstractService {

    protected final Logger logger = LoggerFactory.getLogger(getClass());


    protected int randInt(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

    protected void printMap2D(Map<String, double[][]> input) {
        logger.info("printMap");
        for (String s : input.keySet()) {
            logger.info(s + " -> " + Arrays.deepToString(input.get(s)));
        }
    }

    protected void printMap(Map<String, double[]> input) {
        logger.info("printMap");
        for (String s : input.keySet()) {
            logger.info(s + " -> " + Arrays.toString(input.get(s)));
        }
    }

    protected void printMapOfStrings(Map<String, String> input) {
        logger.info("printMapOfStrings");
        for (String s : input.keySet()) {
            logger.info(s + " = " + input.get(s));
        }
    }

    protected void printDistancesMap(Map<Double,Map<String, String>> input) {
        logger.info("printDistancesMap");
        for (Double dist : input.keySet()) {
            logger.info(dist + " -> ");
            printMapOfStrings(input.get(dist));
        }
    }

    protected boolean isNumeric(String str)
    {
        try
        {
            double d = Long.parseLong(str);
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
        return true;
    }

}
