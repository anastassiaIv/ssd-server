package ee.ssd.api.service;

import ee.ssd.api.data.entity.Course;
import ee.ssd.api.data.entity.Similarity;
import ee.ssd.api.service.material.ConvertTextToNumericFormatService;
import ee.ssd.api.service.material.CosineSimilarityService;
import ee.ssd.api.service.material.WebScraperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.TimeUnit;


@Service("CourseSimilaritiesService")
public class CourseSimilaritiesService extends AbstractService {

    @Autowired
    private WebScraperService webScraperService;

    @Autowired
    private RunPythonCodeService runPythonCodeService;

    @Autowired
    private FileHandleService fileHandleService;

    @Autowired
    private ConvertTextToNumericFormatService convertTextToNumericFormatService;

   @Autowired
   private CosineSimilarityService cosineSimilarityService;

    public List<Similarity> findAll(String code) throws IOException {
        logger.info("CODE: " + code);
        List<Similarity> result = new ArrayList<>();
        logger.info("scraping courses...");
        List<Course> courses = webScraperService.getAllCourses();
        logger.info("done. continuing...");
        Map<String, List<String>> tokens = new HashMap<>();
        logger.info("STEP 1: web scraping & tokenization");
        long step1start = System.currentTimeMillis();
        for (Course c : courses) {
            if (fileHandleService.ifFileAlreadyExists(c.getCode())) {
                logger.info("course code: " + c.getCode());
                String tokensStringFromFile = fileHandleService.readFile(c.getCode(), StandardCharsets.UTF_8);
                logger.info("tokensStringFromFile: " + tokensStringFromFile);
                List<String> lectureTokens = Arrays.asList(tokensStringFromFile.split(","));
                logger.info("lectureTokens: " + lectureTokens.size());

                int halfTokens2 = (int) lectureTokens.size() / 2;
                List<String> chosen = new ArrayList<>();
                for (int i = 0; i < halfTokens2; i++) {
                    chosen.add(lectureTokens.get(randInt(0, lectureTokens.size()-1)));

                }
                logger.info("total: " + chosen.size());
                logger.info("code: " + code);
                if (c.getCode().trim().contains(code.trim())) {
                    tokens.put(code, chosen);
                } else {
                    tokens.put(c.getCode(), chosen);

                }
            }
            else {
                logger.info("course file not found: " +  c.getCode());
                List<String> lectureTokens = new ArrayList<>();
                try {
                    lectureTokens = runPythonCodeService.getTokens(c.getCode());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                tokens.put(c.getCode(), lectureTokens);
                logger.info("Creating file to keep tokens");
                fileHandleService.createFile(code);

                logger.info("Writing tokens into file");
                String joined = String.join(",", lectureTokens); // "foo and bar and baz"
                fileHandleService.writeIntoFile(code, joined);
                logger.info("Finished");
            }

        }
        long step1end = System.currentTimeMillis();
        logger.info("STEP 1 TIME: " +  TimeUnit.MILLISECONDS.toSeconds(step1end - step1start) +  " sek");
        logger.info("STEP 2: Cosine similarity");
        List<String> originalInput = tokens.get(code);
        logger.info("originalInput: " + originalInput);
        if (originalInput != null) {
            logger.info("tokens.keySet(): " + tokens.keySet().size());
            for (String c : tokens.keySet()) {
                logger.info("code: " + c);
                if (!c.trim().equals(code.trim()) && tokens.get(c).size() > 0) {
                    logger.info("calculating similarity");
                    Similarity s = new Similarity();
                    logger.info("original: "  + String.join(" ", originalInput));
                    logger.info("candidate: " + String.join(" ", tokens.get(c)));

                    double[] originalBinary = convertTextToNumericFormatService.convertCurrentMaterialToBinaryForCosine(originalInput, tokens.get(c));
                    Map<String, Integer> candidateTokensFreq = convertTextToNumericFormatService.countFreqForCosine(originalInput, tokens.get(c), false);
                    double[] candidateBinary = convertTextToNumericFormatService.createBinaryVectorForCosine(candidateTokensFreq);

                    logger.info("originalBinary: " + originalBinary.length);
                    logger.info("candidateBinary: " + candidateBinary.length);

                    if (originalBinary.length != candidateBinary.length) {
                        if (originalBinary.length > candidateBinary.length) {
                            originalBinary = Arrays.copyOf(originalBinary, candidateBinary.length);
                        } else {
                            candidateBinary = Arrays.copyOf(candidateBinary, originalBinary.length);
                        }
                    }

                    logger.info("originalBinary: " + Arrays.toString(originalBinary));
                    logger.info("candidateBinary: " + Arrays.toString(candidateBinary));

                    double similarity = cosineSimilarityService.cosineSimilarity(originalBinary, candidateBinary);
                    s.setCourse(c.trim());
                    s.setSimilarity( similarity * 100);
                    logger.info("similarity: " + similarity + " - " + similarity * 100);
                    result.add(s);
                }
            }
        } else {
            logger.info("Tokens for original input " + code + " were not found");
        }
        logger.info("result: " + result.size());
        return result;
    }

}
