package ee.ssd.api.service;

import ee.ssd.api.data.entity.Material;
import org.apache.commons.io.FileUtils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Service("FileHandleService")
public class FileHandleService extends AbstractService {

    @Value("${tokensLocation}")
    private String tokensLocation;

    @Value("${materialsLocation}")
    private String materialsLocation;

    public void createFile(String fileName) {
        logger.info("Create file: " + tokensLocation + "/" + fileName + ".txt");
        Path path = Paths.get(tokensLocation + "/" + fileName + ".txt");

        if (!ifFileAlreadyExists(fileName)) {
            try {
                logger.info("creating file");
                Files.createDirectories(path.getParent());
            } catch (IOException e) {
                logger.error("IO eexcpetion: " + e.getMessage());
            }
        }
    }

    public void createMaterialsFile(String fileName) {
        logger.info("Create file: " + materialsLocation + "/" + fileName + ".txt");
        Path path = Paths.get(materialsLocation + "/" + fileName + ".txt");

        if (!ifFileAlreadyExists(fileName)) {
            try {
                logger.info("creating file");
                Files.createDirectories(path.getParent());
            } catch (IOException e) {
                logger.error("IO eexcpetion: " + e.getMessage());
            }
        }
    }

    public boolean ifFileAlreadyExists(String fileName) {
        String path = tokensLocation + "/" + fileName + ".txt";
        logger.info("ifFileAlreadyExists: " + path);
        boolean check = new File(path).exists() && new File(path).length() > 0;
        return check;
    }

    public void writeIntoFile(String fileName, String str) throws IOException {
            BufferedWriter writer = new BufferedWriter(new FileWriter(tokensLocation + "/" + fileName + ".txt"));
            writer.write(str);
            writer.close();
    }

    public void writeIntoFileMaterials(String fileName, String str) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(materialsLocation + "/" + fileName + ".txt"));
        writer.write(str);
        logger.info("str: " + str);
        writer.close();
    }

    public String readFile(String code, Charset encoding)
            throws IOException
    {
        byte[] encoded = Files.readAllBytes(Paths.get(tokensLocation + "/" + code + ".txt"));
        return new String(encoded, encoding);
    }

    public boolean materialsExist(String code) {
        String path = materialsLocation + "/" + code + ".txt";
        logger.info("materialsExist: " + path);
        boolean check = new File(path).exists();
        return check;
    }

    public List<Material> getMaterialsFromFile( String code) throws IOException {
        List<Material> result = new ArrayList<>();
        byte[] encoded = Files.readAllBytes(Paths.get(materialsLocation + "/" + code + ".txt"));
        String str =  new String(encoded, StandardCharsets.UTF_8);
        String[] arr = str.split("-----------------");
        for (String s : arr) {
            if (s != null && s.trim().length() > 0) {
                Material material = new Material();
                logger.info("s: " + s);
                String title = s.split("###")[0].replaceAll("STARTTITLE:\\{\\{", "")
                        .replaceAll("}}ENDTITLE", "");
                material.setTitle(title);
                String link = s.split("###")[1].replaceAll("STARTLINK:\\{\\{", "")
                        .replaceAll("}}ENDLINK", "");
                material.setLink(link);
                result.add(material);
            }
        }
        return result;
    }

    public void clearDirecotry(String dir) throws IOException {
        FileUtils.cleanDirectory(new File(dir));

    }

}
