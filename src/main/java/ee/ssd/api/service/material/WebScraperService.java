package ee.ssd.api.service.material;

import com.itextpdf.text.exceptions.InvalidPdfException;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import ee.ssd.api.data.entity.Course;
import ee.ssd.api.service.AbstractService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.*;

/**
 * Created by anastassia on 30/06/2017.
 *
 * Klass courses.cs.ut.ee materjali kättesaamiseks - pdf ja pealirjad hetkel
 *
 * STEP 1
 */


@Service("WebScraperService")
public class WebScraperService extends AbstractService {

    @Autowired
    private Environment environment;

    @Autowired
    private PdfExtractorService pdfExtractorService;

    public  String read(String link, String name) {
        StringBuilder finalText = new StringBuilder();
        logger.info("Reading: " +  link);
        String text = "";
        try {
            PdfReader reader = new PdfReader(new URL(link));

            for (int i = 1; i <= reader.getNumberOfPages(); i++) {
                //text += PdfTextExtractor.getTextFromPage(reader, i);
                //ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                String currentText = "";
                try {
                    PdfTextExtractor.getTextFromPage(reader, i);
                } catch (NumberFormatException e) {
                    logger.error(e.getMessage());
                }

                //currentText = Encoding.UTF8.GetString(ASCIIEncoding.Convert(Encoding.Default, Encoding.UTF8, Encoding.Default.GetBytes(currentText)));
                //text.Append(currentText);
                byte[] bites = currentText.getBytes(Charset.forName("UTF-8"));
                logger.debug("AINE STRING: " + ( new String(bites, "UTF-8")));
                finalText.append(new String(bites, "UTF-8"));
            }

            logger.debug("DOWNLOADED: " +  finalText.toString().length() + ", " +  finalText.toString());
            reader.close();


        } catch (InvalidPdfException e) {
            logger.error("Invalid pdf");
            Document context = null;
            try {
                 context =  Jsoup.connect(link).get();
            } catch (IOException e1) {
                logger.error("Viga artikli lugemisel..." + e1.getMessage());
                //TODO:saab paremini?
                return "";
            }
            return context.text().replaceAll("\\r\\n|\\r|\\n", " ").trim().replaceAll(" +", " ").replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();


        } catch (IllegalArgumentException e) {
            logger.info("Invalid pdf");
            Document context = null;
            try {
                context =  Jsoup.connect(link).get();
            } catch (IOException e1) {
                logger.error("Viga artikli lugemisel... " + e1.getMessage());
                //TODO:saab paremini?
                return "";
            }
            return context.text().replaceAll("\\r\\n|\\r|\\n", " ").trim().replaceAll(" +", " ").replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();

        } catch (MalformedURLException e) {
            logger.error("Viga artikli lugemisel..." + e.getMessage());
            //TODO:saab paremini?
            return "";
        } catch (IOException e) {
            logger.error("Viga artikli lugemisel..." + e.getMessage());
            //TODO:saab paremini?
            return "";
        }


        logger.info("Done.");
        return finalText.toString();

    }

    public String readFromPdf(String link) {
        return pdfExtractorService.processRecord(link);
    }

    private String getCurrentSemester() {
        int year = Calendar.getInstance().get(Calendar.YEAR);
        int month = Calendar.getInstance().get(Calendar.MONTH) + 1;
        String sem = "";
        if (month > 1 && month < 9) {
            sem = "S";
        } else {
            sem = "F";
        }
        return String.valueOf(year) + sem;
    }


    public List<Course> getAllCourses() {
        List<Course> result = new ArrayList<>();
        logger.info("Current semestr.");
        String url = environment.getProperty("coursesUrl");
        //Ühendan courses.cs.ut.ee
        Document doc = null;

        boolean isSpring = getCurrentSemester().contains("S");

        try {
            doc = Jsoup.connect(url).get();
        } catch (IOException e) {
            logger.error("Viga courses.cs.ut.ee ühendamisega");
        }

        //valin semestrid
        Elements nav = doc.getElementsByClass("sidebar");
        Elements links = nav.select("a[href]");


        for (Element link : links) {
           // logger.info("link: " + link.attr("href"));
            // logger.info("text: " + link.text());
        }


        //hakame ained jooksvast semestrist

        Document currentSemestr = null;

        try {
            if (isSpring && ( url + links.get(0).attr("href")).contains("fall")) {
                currentSemestr = Jsoup.connect(url + links.get(1).attr("href")).get();
            } else {
                currentSemestr = Jsoup.connect(url + links.get(0).attr("href")).get();
            }
        } catch (IOException e) {
            logger.info("Viga courses.cs.ut.ee ühendamisega");
        }
        if (isSpring && ( url + links.get(0).attr("href")).contains("fall")) {
            logger.info("Vaatan: " + ( url + links.get(1).attr("href")));
        } else {
            logger.info("Vaatan: " + ( url + links.get(0).attr("href")));
        }
        logger.info("Pärin selle semestri ained...");
        Elements currentSemCourses = currentSemestr.getElementsByClass("course-list");
        Elements ained = currentSemCourses.select("a[href]");


        for (Element link : ained) {
           // logger.info("link: " + link.attr("href"));
           // logger.info("text: " + link.text());
            String courseUrl = url + link.attr("href");
            if (isNumeric(link.attr("href").split("/")[1])) {
                String code = link.text().split(" ")[0];
                String name = "";
                for (int i = 1; i <  link.text().split(" ").length; i++) {
                    name = name + link.text().split(" ")[i];
                }
                Course course = new Course(code, name, courseUrl);
                result.add(course);
            }
        }

        return result;
    }
}
