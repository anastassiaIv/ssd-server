package ee.ssd.api.service.material;

import ee.ssd.api.service.AbstractService;
import eu.hlavki.text.lemmagen.LemmatizerFactory;
import eu.hlavki.text.lemmagen.api.Lemmatizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

/**
 * Created by anastassia on 03/07/2017.
 *
 * tokenize article to get features.
 * Cleaning process STEP 1
 */

@Service("TokenizationService")
public class TokenizationService  extends AbstractService {

    @Autowired
    private DetectLanguageService detectLanguageService;

    //Kuna Python koodis mõned tokenid on valesti kodeeritud, siis tuleb teha lisa puhastust ja mitte kasutada selliseid  analüüsis

    public List<String> cleanOriginalInputTokens(List<String> originalInput) {
        logger.info("cleanOriginalInoutTokens: " + originalInput.size());
        List<String> result = new ArrayList<>();
        for (String s : originalInput) {
            if (!s.matches(".*\\d+.*")) {
                result.add(s);
            }
        }
        logger.info("Result: " + result.size());
        return result;

    }

    public List<String> getTokens(String candidate) {
        logger.info("Starting tokenization... ");

        //remove linea breaks && multiple spaces && punctuation
        candidate = candidate.replaceAll("\\r\\n|\\r|\\n", " ").trim().replaceAll(" +", " ").replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();

        //TODO: kontrollida üle

        String[] tokens = candidate.split("\\s+");
        logger.info("TEKST: " +  tokens.length);

        List<String> list = removeTooShortValuesAndOnlydigits(tokens);

        String joinedList = String.join(",", list);
        String language = detectLanguageService.detectLanguage(joinedList);
        logger.info("language: " + language);

        List<String> engLemmas = new ArrayList<>();
        List<String> estLemmas = new ArrayList<>();

        if (language.equals("en")) {
            // english lemmatization
            // https://github.com/hlavki/jlemmagen
            try {
                Lemmatizer lm = LemmatizerFactory.getPrebuilt("mlteast-en");
                for (String word : list) {
                    engLemmas.add(lm.lemmatize(word).toString());
                    logger.info("word: " + word + ", lemma: " + lm.lemmatize(word).toString());

                }
            } catch (IOException e) {
                logger.info("Problem with lemmatizer: " + e.getMessage());
            }
        } else if (language.equals("et")) {
            // estonian lemmatization
            try {
                Lemmatizer lm = LemmatizerFactory.getPrebuilt("mlteast-et");
                for (String word : list) {
                    estLemmas.add(lm.lemmatize(word).toString());
                    logger.info("word: " + word + ", lemma: " + lm.lemmatize(word).toString());

                }
            } catch (IOException e) {
                logger.info("Problem with lemmatizer: " + e.getMessage());
            }
        }

        List<String> lemmas = engLemmas;
        lemmas.addAll(estLemmas);

        // removing duplicates from lemmas list
        Set<String> hs = new HashSet<>();
        hs.addAll(lemmas);
        lemmas.clear();
        lemmas.addAll(hs);
        logger.info("left lemmas: " +  lemmas.size());

        if (engLemmas.size() > 0 || estLemmas.size() > 0) {
            return lemmas;
        }

        // removing duplicates from lemmas list
        Set<String> hs2 = new HashSet<>();
        hs2.addAll(list);
        lemmas.clear();
        lemmas.addAll(list);
        logger.info("left list: " +  list.size());

        return list;

    }

    private List<String> removeTooShortValuesAndOnlydigits(String[] tokens) {
        logger.info("removeTooShortValuesAndOnlydigits");
        List<String> result = new ArrayList<>();
        for (String s : tokens) {
            logger.debug("TOKEN: " + ", " +  s + ", " + (s.length() > 5) + ", " + !isNumeric(s) + ", " +  (countDigits(s) < (s.length() - countDigits(s))) + ", " + !s.contains("mtat") );
            if (s.length() > 5 && !isNumeric(s) && countDigits(s) < (s.length() - countDigits(s)) && !s.contains("mtat")) {
                result.add(s);
            }
        }
        logger.info("Result: " + result.size());
        return result;
    }


    private int countDigits(String s) {
        int count = 0;
        for (int i = 0, len = s.length(); i < len; i++) {
            if (Character.isDigit(s.charAt(i))) {
                count++;
            }
        }
        return count;
    }


}
