package ee.ssd.api.service.material;

import ee.ssd.api.service.AbstractService;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.sax.BodyContentHandler;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;

@Service("PdfExtractorService")
public class PdfExtractorService extends AbstractService {

    // https://stackoverflow.com/questions/41418881/parsing-text-and-images-from-pdf

    public String processRecord(String url) {
        HttpClient httpclient = HttpClientBuilder.create().build();
        String text = "";
        try {
            HttpGet httpGet = new HttpGet(url);
            HttpResponse response = httpclient.execute(httpGet);
            HttpEntity entity = response.getEntity();
            InputStream input = null;
            if (entity != null) {
                try {
                    input = entity.getContent();
                    BodyContentHandler handler = new BodyContentHandler(-1);
                    Metadata metadata = new Metadata();
                    AutoDetectParser parser = new AutoDetectParser();
                    ParseContext parseContext = new ParseContext();
                    parser.parse(input, handler, metadata, parseContext);
                    text = handler.toString().replaceAll("\n|\r|\t", " ");
                    // metadata.get(TikaCoreProperties.TITLE)

                } catch (Exception e) {
                    logger.error("" + e.getMessage());

                } finally {
                    if (input != null) {
                        try {
                            input.close();
                        } catch (IOException e) {
                            logger.error("" + e.getMessage());

                        }
                    }
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return String.valueOf(text);
    }
}
