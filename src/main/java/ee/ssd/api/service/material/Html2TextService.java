package ee.ssd.api.service.material;

import ee.ssd.api.service.AbstractService;
import org.jsoup.Jsoup;
import org.springframework.stereotype.Service;

@Service("Html2TextService")
public class Html2TextService extends AbstractService {

    // https://stackoverflow.com/questions/240546/remove-html-tags-from-a-string

    public  String html2text(String html) {
        return Jsoup.parse(html).text();
    }

}
