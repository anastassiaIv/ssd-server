package ee.ssd.api.service.material;
import ee.ssd.api.service.AbstractService;
import org.apache.tika.language.LanguageIdentifier;
import org.springframework.stereotype.Service;

@Service("DetectLanguageService")
public class DetectLanguageService extends AbstractService {

    // http://tika.apache.org/1.5/detection.html#Language_Detection

    public  String detectLanguage(String input) {
        LanguageIdentifier identifier = new LanguageIdentifier(input);
        String language = identifier.getLanguage();
        return language;
    }

}
