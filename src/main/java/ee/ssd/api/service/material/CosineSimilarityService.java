package ee.ssd.api.service.material;

import ee.ssd.api.service.AbstractService;
import org.springframework.stereotype.Service;

/**
 * Created by anastassia on 11/02/2019.
 *
 * STEP 5 - measure similarity between two articles
 *
 * iga double[] on artikkel kus väärtused on features
 */


@Service("CosineSimilarityService")
public class CosineSimilarityService extends AbstractService {

    // https://stackoverflow.com/questions/520241/how-do-i-calculate-the-cosine-similarity-of-two-vectors

    public double cosineSimilarity(double[] original, double[] candidate) {
        double dotProduct = 0.0;
        double normA = 0.0;
        double normB = 0.0;
        for (int i = 0; i < original.length; i++) {
            dotProduct += original[i] * candidate[i];
            normA += Math.pow(original[i], 2);
            normB += Math.pow(candidate[i], 2);
        }
        return dotProduct / (Math.sqrt(normA) * Math.sqrt(normB));
    }

}
