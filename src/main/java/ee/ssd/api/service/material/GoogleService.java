package ee.ssd.api.service.material;

import ee.ssd.api.service.AbstractService;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by anastassia on 03/07/2017.
 *
 * STEP 0 - search top n google results
 * title-url-context
 */


@Service("GoogleService")
public class GoogleService extends AbstractService {


    @Autowired
    private WebScraperService webScraperService;

    @Autowired
    private Html2TextService html2TextService;

    private final String GOOGLE_SEARCH_URL = "https://www.google.com/search";

    @Value("#{'${validUrls}'.split(',')}")
    private List<String> validUrls;

    @Value("#{'${invalidUrls}'.split(',')}")
    private List<String> invalidUrls;

    //TODO: search only pdf? better for parsing context
    //TODO:kasutada mitte google vaid mingisugust aritklide kogumit? nt lõputööde andmebaasi v http://www.imelineteadus.ee/tehnoloogia

    public Map<String, Map<String, String>> getGoogleResults(int limit, String searchQuery, String code) {
        logger.info("getGoogleResults");
        Map<String, Map<String, String>> results = new HashMap<>();

        String searchURL = GOOGLE_SEARCH_URL + "?q="+searchQuery+"&num="+limit;
        logger.info("Otsing: {}", searchURL);
//
//        Google google = new Google(searchURL);
//        Thread googele_thread = new Thread(google);
//        googele_thread.start();
//
//        try {
//            logger.info("Waiting for thread to finish.");
//            google.w
//        } catch (InterruptedException e) {
//            System.out.println("Main thread Interrupted");
//        }
//
//        Document doc = google.getDocument();

        Document doc = null;
        Connection con = Jsoup.connect(searchURL).userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21").timeout(10000);
        Connection.Response resp = null;
        try {
            resp = con.execute();
        } catch (IOException e) {
            logger.error("Connection respons problem");
            logger.error(e.getMessage());
        }

        if (resp.statusCode() == 200) {
            try {
                doc = con.get();
            } catch (IOException e) {
                logger.error("Viga  otsimisel... ");
                logger.error(e.getMessage());
            }
        }
//        Document doc = null;
//        try {
//            doc = Jsoup.connect(searchURL).userAgent("Chrome").get();
//        } catch (IOException e) {
//            logger.error("Viga  otsimisel... ");
//            logger.error(e.getMessage());
//        }

        //If google search results HTML change the <h3 class="r" to <h3 class="r1"
        //we need to change below accordingly

        if (doc != null) {
            logger.info("scanning");
            Elements links = doc.select("div.r a"); //h3.r > a

            for (Element result : links) {
                String url = result.attr("href");
                logger.info("Leitud google link: {}", url);
                if (validateUrl(url, code)) {
                    //if (url.contains(".pdf") ) {
                    String title = result.text();
                    Document context = null;
                    String tekstKuju = "";
                    if (!url.contains(".pdf")) {
                        try {
                            logger.info("Not pdf");
                            context = Jsoup.connect(url).get();
                            tekstKuju = html2TextService.html2text(context.html());
                            logger.info("tekstKuju: " + tekstKuju.length());


                        } catch (IOException e) {
                            logger.error("Viga artikli lugemisel...");
                        } catch (IllegalArgumentException e) {
                            logger.info("Malform URL");
                        }

                    } else {
                        // tekstKuju = webScraperService.read(url, title);
                        tekstKuju = webScraperService.readFromPdf(url);
                    }

                    logger.info("Leitud google teksti: {}", tekstKuju.length());
                    /*
                    tekstKuju = webScraperService.read(url, title);
                    log.info("Leitud google teksti: {}", tekstKuju.length());
                    */
                    //System.out.println("Text::" + linkText + ", URL::" + linkHref.substring(6, linkHref.indexOf("&")));
                    Map<String, String> info = new HashMap<>();
                    if (context != null) {
                        info.put(url, context.text().replaceAll("\\r\\n|\\r|\\n", " ").trim().replaceAll(" +", " ").replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase());

                    } else {
                        if (!tekstKuju.equals("")) {
                            info.put(url, tekstKuju.replaceAll("\\r\\n|\\r|\\n", " ").trim().replaceAll(" +", " ").replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase());
                        }
                    }

                    if (!title.contains("Tõlgi see leht") && !title.contains("Puhverdatud") && !title.contains("Sarnased")) {
                        info.put(url, tekstKuju);
                        logger.info("Salvestan url ja tekst: " + url + " " + info.size() );
                        results.put(title, info);
                        logger.info("Resulting... Title: " + ", URL: " +  title, url);
                    }

                    /*
                } else {
                    log.info("Leitud google link ei ole pdf.");
                }
                */
                } else {
                    logger.info("Url is not valid.");
                }


            }
        } else {
            logger.error("Ei saanud ühendust google serveriga..." + searchURL);
        }

        return results;

    }

    public boolean validateUrl(String url, String code) {
        logger.info("validateUrl: " + url);
        boolean valid = false;

        if (!url.equals("#")) {
            for (String s : validUrls) {
                logger.info("URL: " + s);
                logger.info("url.contains(s.trim()): " + url.contains(s.trim()));
                if (url.contains(s.trim()) || (url.contains("courses.cs.ut.ee") && !url.contains(code))) {
                    for (String s1 : invalidUrls) {
                        if (!url.contains(s1.trim())) {
                            valid = true;
                            return valid;
                        } else {
                            logger.info("See link on blacklistitud: " + url);
                            valid = false;

                        }
                    }
                } else {
                    logger.info("See link pole whitelistitud: " + url);
                    valid = false;
                }
            }
        }
        return valid;
    }
}