package ee.ssd.api.service.material;

import ee.ssd.api.data.entity.Material;
import ee.ssd.api.service.AbstractService;
import ee.ssd.api.service.FileHandleService;
import ee.ssd.api.service.RunPythonCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created by anastassia on 03/07/2017.
 *
 * STEP 1: collecting tokens for the specified course
 * STEP 1.1: The algorithm checks if the tokens for the course already exist.
 * STEP 1.2: If tokens exist, then the set of tokens is read and the algorithm moves on to
 * STEP 2. If tokens for the specified course have not been found, the algorithm calls
 *         Python method to collect tokens only for the specified course.
 * STEP 1.3: Randomly choosing tokens to use in Google search.
 * STEP 2: google search
 * STEP 3: converting each google article to binary vector
 * STEP 3.1: preparing for step 4. Created a map with essential info
 * STEP 3.2: choosing tokens if necessary
 * STEP 4: computing SVD matrix
 * STEP 5: computing Cosine similarity
 * STEP 5.1: sorting results
 * STEP 6: resulting
 *
 */


@Service("RecommendArticleService")
public class RecommendArticleService extends AbstractService {

    @Autowired
    private GoogleService googleService;

    @Autowired
    private ConvertTextToNumericFormatService convertTextToNumericFormatService;

    @Autowired
    private SVDService svdService;

    @Autowired
    private CosineSimilarityService cosineSimilarityService;

    @Autowired
    private Environment environment;

    @Autowired
    private RunPythonCodeService runPythonCodeService;

    @Autowired
    private FileHandleService fileHandleService;

    @Autowired
    private TokenizationService tokenizationService;

    @Value("${googleTryLimits}")
    private String googleTryLimits;

    public List<Material> getRecommendedArticlesForCourse(String code) throws IOException {
        List<Material> recommendedArticles = new ArrayList<Material>();
        logger.info("getRecommendedArticles: " +  code);
        logger.info("checking for existing recommendations");
        if (fileHandleService.materialsExist(code)) {
            return fileHandleService.getMaterialsFromFile(code);

        } else {
            logger.info("STEP 1: collecting tokens for the specified course");
            long step1start = System.currentTimeMillis();
            long methodStart = System.currentTimeMillis();
            List<String> lectureTokens = new ArrayList<>();
            if (fileHandleService.ifFileAlreadyExists(code)) {
                logger.info("course file found: " + code);
                String tokensStringFromFile = fileHandleService.readFile(code, StandardCharsets.UTF_8);
                lectureTokens = Arrays.asList(tokensStringFromFile.trim().split(","));

                if (tokensStringFromFile.length() == 0 || tokensStringFromFile.trim().length() == 0) {
                    logger.info("tokensStringFromFile list is null or empty. Returning empty array ...");
                    return new ArrayList<>();
                }
            }
            else {
                logger.info("course file not found: " + code);
                try {
                    lectureTokens = runPythonCodeService.getTokens(code);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            long step1end = System.currentTimeMillis();
            logger.info("STEP 1 TIME: " +  TimeUnit.MILLISECONDS.toSeconds(step1end - step1start) +  " sek");
            logger.info("Lecture tokens: " + lectureTokens.size());

            logger.info("STEP 1.2: choosing tokens");
            long step2start = System.currentTimeMillis();

            List<String> cleanedLectureTokens = tokenizationService.cleanOriginalInputTokens(lectureTokens);

            List<String> chosenTokens = new ArrayList<>();

            if (cleanedLectureTokens != null && cleanedLectureTokens.size() > 0) {
                int originalInputChosenTokenForAnalysisLimit = Integer.valueOf(environment.getProperty("originalInputChosenTokenForAnalysisLimit"));
                for (int i = 0; i < originalInputChosenTokenForAnalysisLimit; i++) {
                    chosenTokens.add(cleanedLectureTokens.get(randInt(0, cleanedLectureTokens.size()-1)));
                }
            } else {
                logger.info("cleanedLectureTokens list is null or empty. Returning empty array ...");
                return new ArrayList<>();
            }
            logger.info("Chosen tokens total: " + chosenTokens.size());
            long step2end = System.currentTimeMillis();
            logger.info("STEP 1.2 TIME: " +  TimeUnit.MILLISECONDS.toSeconds(step2end - step2start) +  " sek");

            logger.info("STEP 2: google search");
            logger.info("googleLimit: " + Integer.valueOf(environment.getProperty("googleLimit")));

            //STEP 3 google
            //title - url - text6. Google

            long step3start = System.currentTimeMillis();

            Map<String, Map<String, String>> googleResult = new HashMap<>();
            int g  = 0;
            while (googleResult.size() == 0) {
                logger.info("attempt: " + g);
                if (g < Integer.valueOf(googleTryLimits)) {
                    googleResult = googleService.getGoogleResults(Integer.valueOf(environment.getProperty("googleLimit")), buildSearchQuery(chosenTokens), code );
                    g++;
                } else {
                    logger.info("Google try limits is full. No supplementary material found... Sorry! Returning empty array ...");
                    return new ArrayList<>();
                }
            }

            logger.info("googleResult: " +  googleResult.size());
            long step3end = System.currentTimeMillis();
            logger.info("STEP 2 TIME: " +  TimeUnit.MILLISECONDS.toSeconds(step3end -step3start) + " sek");

            logger.info("STEP 3: converting each google article to binary vector");

            //STEP 4 convert articles info binary vectors
            //title-url-context

            long step4start = System.currentTimeMillis();

            Map<String, Map<String, double[][]>> vectors = new HashMap<>();
            for (String title : googleResult.keySet()) {
                logger.info("googleResult.get(title).keySet(): " );
                googleResult.get(title).keySet().forEach(logger::info);
                logger.info("googleResult.get(title).keySet().toArray() != null: " + (googleResult.get(title).keySet().toArray() != null));
                logger.info("googleResult.get(title).keySet().length: " + (googleResult.get(title).keySet().toArray().length));
                if (googleResult.get(title).keySet().toArray() != null && googleResult.get(title).keySet().toArray().length >0) {
                    String url = String.valueOf(googleResult.get(title).keySet().toArray()[0]);
                    String text = googleResult.get(title).get(googleResult.get(title).keySet().toArray()[0]);
                    logger.info("Title: " + title);
                    logger.info("CHECK: " + title.length());
                    logger.info("Url: " + url);
                    logger.debug("Text: " +  text);
                    List<String> candidateTokens = Arrays.asList(text.split(" "));
                    if (candidateTokens.size() == 0 ) {
                        candidateTokens = chosenTokens;
                    } else {
                        List<String> chosenCandidateTokens = convertTextToNumericFormatService.chooseRandomlyCandidateTokens(candidateTokens);
                        Map<String, Integer> freq = convertTextToNumericFormatService.countFreqForCosine(chosenTokens, chosenCandidateTokens, false);
                        candidateTokens = convertTextToNumericFormatService.cleanTokens(chosenCandidateTokens, freq);
                        if (candidateTokens.size() == 0 ) {
                            candidateTokens = chosenTokens;
                        }
                    }
                    double[] aineCleanedBinaryVector = convertTextToNumericFormatService.convertCurrentMaterialToBinaryForCosine(chosenTokens, candidateTokens);
                    double[] vector = convertTextToNumericFormatService.convertTextToNumericFormat(text, chosenTokens);
                    Map<String, double[][]> urlText = new HashMap<>();
                    double[][] originalAndCandidateVectors = {aineCleanedBinaryVector, vector};
                    urlText.put(url, originalAndCandidateVectors);
                    vectors.put(title, urlText);
                } else {
                    logger.info("Mingil põhjusel artiklist " + title + " ei õnnestunud saada teksti. Seega välistan soovitustest...");
                }

            }

            logger.info("vectors: " + vectors.size());
            long step4end = System.currentTimeMillis();
            logger.info("STEP 3 TIME: " + TimeUnit.MILLISECONDS.toSeconds(step4end  -step4start) +  " sek");


            List<Map<String, double[]>> input = new ArrayList<>();

            logger.info("STEP 3.1: preparing for step 4. Created a map with essential info");
            long step41start = System.currentTimeMillis();

            //STEP 3.1 make title-double map
            //sin koik artiklid mis google pakkus
            for (String title : vectors.keySet()) {
                Map<String, double[]> articles = new HashMap<>();
                double[] aineCleanedBinaryVector = vectors.get(title).get(vectors.get(title).keySet().toArray()[0])[0];
                double[] articelBinary = vectors.get(title).get(vectors.get(title).keySet().toArray()[0])[1];
                //TODO: kas saab paremini?
                logger.info("aineCleanedBinaryVector: " + aineCleanedBinaryVector.length);
                logger.info("articelBinary: " + articelBinary.length);
//                if (articelBinary.length == aineCleanedBinaryVector.length) {
//                    //esimene paariline on current article
//                    articles.put("AINE_MATERJAL", aineCleanedBinaryVector);
//                    articles.put(title, articelBinary);
//                    input.add(articles);
//                }
                if (aineCleanedBinaryVector.length != articelBinary.length) {
                    if (aineCleanedBinaryVector.length > articelBinary.length) {
                        aineCleanedBinaryVector = Arrays.copyOf(aineCleanedBinaryVector, articelBinary.length);
                    } else {
                        articelBinary = Arrays.copyOf(articelBinary, aineCleanedBinaryVector.length);
                    }
                }

                articles.put("AINE_MATERJAL", aineCleanedBinaryVector);
                articles.put(title, articelBinary);
                input.add(articles);

            }
            long step41end = System.currentTimeMillis();
            logger.info("input: " + input.size());
            logger.info("STEP 3.1 TIME: " +  TimeUnit.MILLISECONDS.toSeconds(step41end - step41start) + " sek");


            //STEP 3.2 randomly choose 1000 tokens
            //TODO: is it ok?
            logger.info("STEP 3.2: chooosing tokens if necessary");
            long step42start = System.currentTimeMillis();

            int candidateArticlesLimit = Integer.valueOf(environment.getProperty("candidateArticlesLimit"));
            List<Map<String, double[]>> chosen = new ArrayList<>();

            if (input.size() > candidateArticlesLimit) {

                for (int i = 0; i < candidateArticlesLimit; i++) {
                    if (input.size() > 0) {
                        chosen.add(input.get(randInt(0, input.size()-1)));
                    } else {
                        logger.info("input.size on null");
                        return recommendedArticles;
                    }

                }
            } else {
                chosen = input;
            }

            long step42end = System.currentTimeMillis();
            logger.info("STEP 3.2 TIME: " +  TimeUnit.MILLISECONDS.toSeconds(step42end - step42start) +  " sek");

            logger.info("chosen: " + chosen);
            logger.info("STEP 4: computing SVD matrix");
            // step 4 get SVD matrix
            //hakan võrdlema kahe kaupa kus esimene matrix on pragune artikkel ja teine on kandidaat
            //map koosneb kasest enrtist, üks on praegu artikkel ja teine on google artikkel
            //title - svd
            //iga mapp sisalsab 2 argumenti

            long step5start = System.currentTimeMillis();

            List<Map<String, double[][]>> result = new ArrayList<>();
            for (Map<String, double[]> pair : chosen) {
                logger.info("pair:");
                printMap(pair);
                Map<String, double[][]> svd = svdService.getSvdMatrix(pair);
                logger.info("svd:");
                printMap2D(svd);
                result.add(svd);
            }

            logger.info("Result: " + result);
            long step5end = System.currentTimeMillis();

            logger.info("STEP 4 TIME: " + TimeUnit.MILLISECONDS.toSeconds(step5end - step5start) +  " sek");

            logger.info("STEP 5: computing Cosine similarity" +  result.size());

            //STEP 6 arvutame distancet
            //nüd meilt list kus iga map on paar elementidest (current artikkel ja kandidaat)
            //distance - title - url

            long step6start = System.currentTimeMillis();

            Map<Double,Map<String, String>> distance = new HashMap<>();

            for (Map<String, double[][]> candidate : result) {
                String cand = String.valueOf(candidate.keySet().toArray()[0]);
                logger.debug("Candidate: " +  cand);
                if (candidate.get(cand).length > 2) {
                    double dist = cosineSimilarityService.cosineSimilarity(candidate.get(cand)[0], candidate.get(cand)[1]);
                    Map<String, String> data = new HashMap<>();
                    String title = cand.replace("AINE_MATERJAL --- ","").replaceAll("---", "").trim();
                    logger.debug("Title: " +  title + ", " +  title.trim().length());
                    if (googleResult.get(title) != null) {
                        if (googleResult.get(title).keySet() != null) {
                            if (googleResult.get(title).keySet().toArray() != null) {
                                if (googleResult.get(title).keySet().toArray().length > 0) {
                                    data.put(title, String.valueOf(googleResult.get(title).keySet().toArray()[0]));
                                    distance.put(dist, data);
                                } else {
                                    logger.info("googleResult.get(title).keySet().toArray() on tühi. Ei saa kuvada URL ja tekst.");
                                    data.put(title, null);
                                    distance.put(dist, data);
                                }

                            } else {
                                logger.info("googleResult.get(title).keySet().toArray() on null. Ei saa kuvada URL ja tekst.");
                                data.put(title, null);
                                distance.put(dist, data);
                            }

                        } else {
                            logger.info("googleResult.get(title).keySet() on null. Ei saa kuvada URL ja tekst.");
                            data.put(title, null);
                            distance.put(dist, data);
                        }

                    }
                    else {
                    /*for (String googleTitle : googleResult.keySet()) {
                        if (title.contains(googleTitle)) {
                            data.put(title, String.valueOf(googleResult.get(googleTitle).keySet().toArray()[0]));
                            distance.put(dist, data);
                        } else if (googleTitle.contains(title)) {
                            data.put(title, String.valueOf(googleResult.get(title).keySet().toArray()[0]));
                            distance.put(dist, data);
                        } else {
                            log.info("googleResult.get(title) on null. Ei saa kuvada URL ja tekst.");
                            data.put(title, null);
                            distance.put(dist, data);
                        }
                    }
                    */
                        logger.info("googleResult.get(title) on null. Ei saa kuvada URL ja tekst.");

                    }
                }

            }

            long step6end = System.currentTimeMillis();
            logger.info("STEP 5 TIME: " +  TimeUnit.MILLISECONDS.toSeconds(step6end - step6start) +  " sek");

            logger.info("Distances found... " +  distance.size());
            printDistancesMap(distance);

            if (distance.size() > 0) {
                logger.info("STEP 5.1: sorting results");
                //STEP 6.1 sort by distance
                long step61start = System.currentTimeMillis();

                // Map<Double,Map<String, String>> sorted = new TreeMap<>(distance);
                Map<Double,Map<String, String>> sorted = distance.entrySet().stream()
                        .sorted(Map.Entry.comparingByKey())
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                                (oldValue, newValue) -> oldValue, LinkedHashMap::new));

                long step61end = System.currentTimeMillis();
                logger.info("STEP 5.1 TIME: " +  TimeUnit.MILLISECONDS.toSeconds(step61end  -step61start) + " sek");

                logger.info("STEP 6: resulting");
                //STEP 7 leian top 5 artikleid mis on kõige sarnased
                long step7start = System.currentTimeMillis();

                if (sorted.size() > Integer.valueOf(environment.getProperty("recommendLimit"))) {
                    logger.info("Leitud soovitusi: {}", sorted.size());
                    for (int i = 0; i < Integer.valueOf(environment.getProperty("recommendLimit")); i++) {
                        Material material = new Material();
                        Map<String, String> entry = sorted.get(sorted.keySet().toArray()[i]);
                        String title = String.valueOf(entry.keySet().toArray()[0]);
                        material.setTitle(title);
                        String url = entry.get(title);
                        material.setLink(url);

                        recommendedArticles.add(material);
                    }

                    logger.info("recommendedArticles: {}", recommendedArticles.size());
                } else {
                    for (int i = 0; i < sorted.size(); i++) {
                        logger.info("Leitud soovitusi on vähem kui limiit: " +  sorted.size());
                        //recommendedArticles.add(sorted.get(sorted.keySet().toArray()[i]));
                        Material material = new Material();
                        Map<String, String> entry = sorted.get(sorted.keySet().toArray()[i]);
                        String title = String.valueOf(entry.keySet().toArray()[0]);
                        material.setTitle(title);
                        String url = entry.get(title);
                        material.setLink(url);

                        recommendedArticles.add(material);
                    }
                }
                fileHandleService.createMaterialsFile(code);
                saveMaterials(code, recommendedArticles);
                long step7end = System.currentTimeMillis();
                logger.info("STEP 6 TIME: " +  TimeUnit.MILLISECONDS.toSeconds(step7end - step7start) + " sek");

            } else if (distance.size() == 0){
                logger.info("Soovitusi ei leitud...");
            }

            long methodEnd = System.currentTimeMillis();
            logger.info("TIME: " +  TimeUnit.MILLISECONDS.toSeconds(methodEnd - methodStart) + " sek");


            return recommendedArticles;
        }
    }

    private void saveMaterials(String code, List<Material> recommendedArticles) throws IOException {
        logger.info("Saving materials: " +  recommendedArticles.size());
        String toSave = "";
        int index = 1;
        for (Material material : recommendedArticles) {
            logger.info("Materials: " + index);
            String title = "STARTTITLE:{{" + material.getTitle() + "}}ENDTITLE";
            String link = "STARTLINK:{{" + material.getLink() + "}}ENDLINK";
            toSave = toSave + title + "###" + link + "-----------------";
            index = index + 1;
        }
        logger.info("toSave: " + toSave);
        fileHandleService.writeIntoFileMaterials(code, toSave);

    }

    /**
     * kuna tokenid on nii plaju, siis on vaja piirata otsingu paramteerite arvu
     * seega valin juhuslikult 10(?) tokenit nimekirjast...muidu google crashib
     *
     * TODO: eemaldada liigseid sõnu, nt õppejõudude nimed... ?
     * TODO: qeury with bi -  and trigrams?
     * */


    private String buildSearchQuery(List<String> tokens) {
        StringBuilder sb = new StringBuilder();
        int count = 0;
        while (count < Integer.valueOf(environment.getProperty("tokensLimit"))) {
            if (tokens.size() > 0) {
                String token = tokens.get(randInt(0, tokens.size()-1));
                if (token != null && token.trim().length() > 5) {
                    // logger.info("Key google otsimiseks: " +  token.replaceAll("\\\\", ""));
                    sb.append(token.replaceAll("\\\\", "").trim()
                    );
                    sb.append(" ");
                } else {
                    count--;
                }
                count++;
            } else {
               // logger.info("NB! Tokens list is empty.");
            }

        }
        sb.append("pdf");
        return sb.toString();
    }

}
