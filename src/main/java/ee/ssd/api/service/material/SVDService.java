package ee.ssd.api.service.material;

import ee.ssd.api.service.AbstractService;
import org.ejml.simple.SimpleMatrix;
import org.ejml.simple.SimpleSVD;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by anastassia on 01/07/2017.
 *
 * SVD algorithm
 *
 *
 * __|Doc 1 | Doc 2 | Doc 3
 * ___________________________
 * w1| 1    | 1     | 1
 * ___________________________
 * w2| 0    | 1     | 0
 *
 * title - points
 * .....
 * STEP 4
 */

@Service("SVDService")
public class SVDService extends AbstractService {

    // https://github.com/Cawfree/ejml-f/blob/master/src/org/ejml/simple/SimpleMatrix.java
    // http://ejml.org/javadoc/org/ejml/simple/SimpleMatrix.html
    // http://ejml.org/javadoc/org/ejml/simple/SimpleSVD.html

    public Map<String,double[][]> getSvdMatrix(Map<String,double[]> input) {
        Map<String,double[][]> points = new HashMap<>();

        double[][] array = new double[input.size()][];

        List<double[]> tempAr = new ArrayList<>();
        for (String s : input.keySet()) {
            tempAr.add(input.get(s));
        }

        for (int i = 0; i < tempAr.size(); i++) {
            array[i] = tempAr.get(i);
        }

        SimpleMatrix matrix = new SimpleMatrix(array);
        SimpleSVD svd = matrix.svd();

        SimpleMatrix U = svd.getU();
        SimpleMatrix W = svd.getW();
        SimpleMatrix V = svd.getV();

        SimpleMatrix result = U.mult(W).mult(V);
        double[][] resultArray = new double[result.numCols()][];

        for (int i = 0; i < result.numCols(); i++) {
            //iga vektor on dokument
            SimpleMatrix temp = result.extractVector(false, i);
            double[] vector = new double[temp.getNumElements()];
            for (int j = 0; j < temp.getNumElements(); j++) {
                vector[j] = temp.get(j);
            }

            resultArray[i] = vector;


        }
        points.put(candidateName(input), resultArray);

        return  points;

    }

    private String candidateName(Map<String,double[]> input) {
        String name = "";
        for (String s : input.keySet()) {
            name += s + " --- ";
        }

        return name;
    }

}
