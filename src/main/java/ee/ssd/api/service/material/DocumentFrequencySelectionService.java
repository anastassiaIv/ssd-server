package ee.ssd.api.service.material;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import ee.ssd.api.service.AbstractService;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by anastassia on 01/07/2017.
 *
 * DF selection
 *
 * Cleaning process STEP 2
 *
 * STEP 3
 */

@Service("DocumentFrequencySelectionService")
public class DocumentFrequencySelectionService extends AbstractService {


    public List<String> getCleanedTokens(List<String> input, Map<String, Integer> freq) {
        return removeHighAndLowFreq(removeStopWords(input), freq);
    }


    public List<String> removeStopWords(List<String> input) {
        logger.debug("Input: " +  input.size());
        List<String> stopwords = new ArrayList<>();

        doCleaning("stopwords/stopwords_en.json", stopwords);

        logger.debug("stopwords: " +  stopwords.size());
        doCleaning("stopwords/stopwords_et.json", stopwords);
        logger.debug("stopwords: " + stopwords.size());

        List<String> cleaned = new ArrayList<>();
        for (String s : input) {
            //check single words
            if (s.split(" ").length == 1) {
                if (!stopwords.contains(s.trim().toLowerCase())) {
                    cleaned.add(s.toLowerCase());
                }
            } //Check bi and trigrams
            else {
                String cleanedString = "";
                for (String s1 : s.split("\\s+")) {
                    if (!stopwords.contains(s1.trim().toLowerCase())) {
                        cleanedString += s1.toLowerCase().trim() + " ";
                    }
                }
                cleaned.add(cleanedString.toLowerCase());
            }
        }
        return cleaned;
    }

    private List<String> removeHighAndLowFreq(List<String> tokens, Map<String, Integer> freq){
        logger.info("removeHighAndLowFreq: " + freq.size());
        Double maxFreq = new Double(Collections.max(freq.values()));
        double minFreq = Collections.min(freq.values());

        Double x = new Double(maxFreq * 0.2);
        int highLimit = x.intValue();

        Double y = new Double(minFreq * 0.2);
        int lowLimit = y.intValue();

        List<String> cleaned = new ArrayList<>();

        if (freq != null) {
            for (String s : tokens) {
                // logger.info("s: " + s);
                if (s != null) {
                    if ((freq.get(s)!= null &&
                            freq.get(s)
                            >= lowLimit)
                            && (freq.get(s)
                            <= highLimit)) {
                        cleaned.add(s);
                    }
                }
            }
        } else {
            logger.error("Freq on null");
        }

        logger.info("Cleaned: " + cleaned.size());

        return cleaned;

    }

    private void doCleaning(String stopwords, List<String> cleaned) {
        JsonParser jsonParser = new JsonParser();
        Object obj = null;//jsonParser.parse(new FileReader(stopwords));
        try {
            InputStream resource = new ClassPathResource(
                    stopwords).getInputStream();
            obj = jsonParser.parse(new InputStreamReader(resource));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            logger.error("Viga json faili ei leitud");
        }
        JsonObject jsonObject = (JsonObject) obj;
        JsonArray words = (JsonArray) jsonObject.get("stopwords");
        logger.debug("sõnu: " +  words.size());
        for (int i = 0; i < words.size(); i++) {
            cleaned.add(String.valueOf(words.get(i)));
        }
    }
}
