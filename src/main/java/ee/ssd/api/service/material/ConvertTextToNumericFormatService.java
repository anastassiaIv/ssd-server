package ee.ssd.api.service.material;

import ee.ssd.api.service.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.*;


/**
 * Created by anastassia on 30/06/2017.
 *
 * STEP 2
 */

@Service("ConvertTextToNumericFormatService")
public class ConvertTextToNumericFormatService extends AbstractService {

    @Autowired
    private TokenizationService tokenizationService;


    @Autowired
    private DocumentFrequencySelectionService documentFrequencySelectionService;

    @Autowired
    private Environment environment;


    //põhimeetod mis tagastab vector-i puhtadest tokenitest

    public double[] convertTextToNumericFormat(String article, List<String> allTokens) {
        logger.info("Cleaned tokens for FP algorithm: " +  allTokens.size());

        //1 tokenite töötlemine

        List<String> processedCandidateTokens = tokenizationService.getTokens(article);

        //Step 2 token frequency counts

        Map<String, Integer> ref = countFreqForCosine(allTokens, processedCandidateTokens, false);

        List<String> cleanedTokens = cleanTokens(allTokens, ref);

        List<String> chosenTokens = chooseRandomlyTokens(cleanedTokens);

        Map<String, Integer> uusRef = countFreqForCosine(allTokens, chosenTokens, false);

        //Step 3 token weights
        //Feature Presence (FP)

        logger.info("Final tokens set: " +  uusRef.size());
        double[] vector = createBinaryVectorForCosine(uusRef);
        logger.info("Binary vector size: " +  vector.length);

        return vector;
    }

    public List<String> cleanTokens(List<String> tokens, Map<String, Integer> freqRef) {
        List<String> cleanedTokens = documentFrequencySelectionService.getCleanedTokens(tokens, freqRef);
        return cleanedTokens;
    }

    public List<String> chooseRandomlyTokens(List<String> input) {
        List<String> chosenTokens = new ArrayList<>();
        int originalInputChosenTokenForAnalysisLimit = Integer.valueOf(environment.getProperty("originalInputChosenTokenForAnalysisLimit"));
        for (int i = 0; i < originalInputChosenTokenForAnalysisLimit; i++) {
            chosenTokens.add(input.get(randInt(0, input.size()-1)));
        }
        return chosenTokens;
    }

    public List<String> chooseRandomlyCandidateTokens(List<String> input) {
        List<String> chosenTokens = new ArrayList<>();
        int candidateInputChosenTokenForAnalysisLimit = Integer.valueOf(environment.getProperty("candidatelInputChosenTokenForAnalysisLimit"));
        for (int i = 0; i < candidateInputChosenTokenForAnalysisLimit; i++) {
            chosenTokens.add(input.get(randInt(0, input.size()-1)));
        }
        return chosenTokens;
    }

    public Map<String, Integer> countFreqForCosine(List<String> finalTokens, List<String> article, boolean forOriginal) {
        logger.info("countFreqForCosine. Original: " + finalTokens.size() + ", Candidate: " + article.size());
        Map<String, Integer> freq = new HashMap<>();
        Set<String> original = new HashSet<String>(finalTokens);
        Set<String> candidate = new HashSet<String>(article);
        Set<String> distinct = new HashSet<String>();
        distinct.addAll(original);
        distinct.addAll(candidate);
        List<String> distinctList = new ArrayList<>(distinct);
        for (int i = 0; i < distinctList.size(); i++) {
            int f = 0;
            if (!forOriginal) {
                f = Collections.frequency(article, distinctList.get(i));

            } else {
                f = Collections.frequency(finalTokens, distinctList.get(i));
            }
            freq.put(distinctList.get(i), f);
        }
        logger.info("Result: " + freq.size());
        return freq;
    }

    public double[] convertCurrentMaterialToBinaryForCosine(List<String> original, List<String> candidate) {
        logger.info("convertCurrentMaterialToBinaryForCosine: " +  original.size() + ", candidate: " + candidate.size());
        List<String> chosenTokens = chooseRandomlyTokens(candidate);
        Map<String, Integer> ref = countFreqForCosine(original, chosenTokens, true);

        double[] vector = createBinaryVectorForCosine(ref);
        logger.info("Binary vector size: " +  vector.length);
        return vector;
    }

    public double[] createBinaryVectorForCosine(Map<String, Integer> tokens) {
        double[] vector = new double[tokens.size()];
        int i = 0;
        for (String s : tokens.keySet()) {
            if (tokens.get(s) != null) {
                vector[i] = tokens.get(s);
            }

            i++;
        }
        return vector;
    }

}
