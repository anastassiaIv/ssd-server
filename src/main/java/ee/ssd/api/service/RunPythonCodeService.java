package ee.ssd.api.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.*;

@Service("RunPythonCodeService")
public class RunPythonCodeService extends AbstractService {

    @Autowired
    private Environment environment;

    @Autowired
    private FileHandleService fileHandleService;

    private static final Charset UTF_8 = Charset.forName("UTF-8");

    public void getTokensForAllCourses() throws IOException {
        String semester = getCurrentSemester();
        logger.info("Scraping all tokens: " + semester);
        Map<String, String> result = new HashMap<>();
        List<String> tokens = new ArrayList<>();
        String temp = "";
        String pythonCodeLocation = environment.getProperty("pythonCodeLocation");
        Runtime rt = Runtime.getRuntime();
        String cmd = "";
        Process pr = null;

        // ##### STEP1: scraping #####

        //courses

        cmd = "make scrape-courses SEMESTERS=" + semester;
        String[] cmds1 ={"/bin/sh", "-c","cd " + pythonCodeLocation + " && " +  cmd};
        logger.info("Ready to scrape courses");
        logger.info("Running: " + String.join(" ", cmds1));
        executeProcess(pr, rt, cmds1, false);

        readProcessOutput(pr);

        pr = null;
        rt = Runtime.getRuntime();

        // moodle

//        cmd = "make scrape-moodle";
//        String[] cmds11 ={"/bin/sh", "-c","cd " + pythonCodeLocation + " && " +  cmd};
//        logger.info("Ready to scrape moodle");
//        logger.info("Running: " + cmd);
//        executeProcess(pr, rt, cmds11
//                , false);
//
//        readProcessOutput(pr);
//
//        pr = null;
//        rt = Runtime.getRuntime();

        // ##### STEP2: extracting #####

        cmd = "python -m extraction.TextExtractor";
        String[] cmds2 = {"/bin/sh", "-c","cd " + pythonCodeLocation + " && " +  cmd};
        logger.info("Running: " + String.join(" ", cmds2));

        executeProcess(pr, rt, cmds2, false);

        readProcessOutput(pr);

        pr = null;
        rt = Runtime.getRuntime();

        // ##### STEP3: tokenization #####

        cmd = "python -m cleaning.Tokenizer";
        String[] cmds3 = {"/bin/sh", "-c","cd " + pythonCodeLocation + " && " +  cmd};
        logger.info("Running: " +  String.join(" ", cmds3));

        temp = executeProcess(pr, rt, cmds3, true);

        // #### STEP4: processsing ####

        if (temp.length() > 0) {
            String dict = temp.split("course_data_tokens ")[1];
            String[] course_pairs = dict.split("u'###");
            for (int i = 1; i < course_pairs.length; i++) {
                String course_code = course_pairs[i].split("': \\[")[0];
                String array = course_pairs[i].split("': \\[")[1].replace("], ", "");

                logger.info("course_code: " + course_code);
                logger.info("array: " + array);
                logger.info("------");

                // processing dict

                String tokensMap = array.replace("\\{", "").replace("}", "");
                logger.info("map: " + tokensMap);
                String[] tokensArray = tokensMap.split(",");
                for (String tokenPair : tokensArray) {
                    if (tokenPair.trim().length() > 1) {
                        String count = tokenPair.split(":")[1];
                        String token = tokenPair.split(":")[0].replace("u'","").replace("'", "");
                        // logger.info("token: " + forceUtf8Coding(token) + ", count: " + count);
                        result.put(course_code, String.join(",", tokens));
                        tokens.add(forceUtf8Coding(token.trim()));
                    }
                }

                logger.info("Total tokens: "+ tokens.size());
                if (tokens.size() > 0) {
                    logger.info("Creating file to keep tokens");
                    fileHandleService.createFile(course_code);

                    logger.info("Writing tokens into file");
                    String joined = String.join(",", tokens); // "foo and bar and baz"
                    fileHandleService.writeIntoFile(course_code, joined);
                } else {
                    logger.info("Not tokens collected.");
                }

                logger.info("Finished with the course: " + course_code);

            }

        }
        logger.info("Finished");

    }

    public void cleanDb() {
        String pythonCodeLocation = environment.getProperty("pythonCodeLocation");
        Runtime rt = Runtime.getRuntime();
        String cmd = "";
        Process pr = null;
        cmd = "python cleandb.py";
        logger.info("Running: " + cmd);
        String[] cmds = {"/bin/sh", "-c","cd " + pythonCodeLocation + " && " +  cmd};
        executeProcess(pr, rt, cmds, true);
    }

    public void cleanRawData() {
        String pythonCodeLocation = environment.getProperty("pythonCodeLocation");
        Runtime rt = Runtime.getRuntime();
        String cmd = "";
        Process pr = null;
        cmd = "make clean-stale";
        logger.info("Running: " + cmd);
        String[] cmds = {"/bin/sh", "-c","cd " + pythonCodeLocation + " && " +  cmd};
        executeProcess(pr, rt, cmds, true);
    }

    public List<String> getTokens(String course) throws IOException {
        logger.info("python code: " + environment.getProperty("pythonCodeLocation"));
        Map<Integer, String> result = new HashMap<>();
        List<String> tokens = new ArrayList<>();
        String temp = "";
        String pythonCodeLocation = environment.getProperty("pythonCodeLocation");
        Runtime rt = Runtime.getRuntime();
        String cmd = "";
        String semester = getCurrentSemester();

        Process pr = null;
//        cmd = "pip install -r requirements.txt";
//        String[] cmds={"/bin/sh", "-c","cd " + pythonCodeLocation + " && " +  cmd};
//        executeProcess(pr, rt, cmds);
//
//        readProcessOutput(pr);

        cmd = "make scrape-course SEMESTERS=" + semester + " COURSE_CODE=" + course;
        String[] cmds1 ={"/bin/sh", "-c","cd " + pythonCodeLocation + " && " +  cmd};
        logger.info("Ready to scrape course");
        logger.info("Running: " + String.join(" ", cmds1));
        executeProcess(pr, rt, cmds1, false);

        readProcessOutput(pr);

        pr = null;
        rt = Runtime.getRuntime();

        cmd = "python -m extraction.TextExtractor";
        String[] cmds2 = {"/bin/sh", "-c","cd " + pythonCodeLocation + " && " +  cmd};
        logger.info("Running: " + String.join(" ", cmds2));

        executeProcess(pr, rt, cmds2, false);

        readProcessOutput(pr);

        pr = null;
        rt = Runtime.getRuntime();

        cmd = "python -m cleaning.Tokenizer";
        String[] cmds3 = {"/bin/sh", "-c","cd " + pythonCodeLocation + " && " +  cmd};
        logger.info("Running: " + String.join(" ", cmds3));

        temp = executeProcess(pr, rt, cmds3, false);

//        if (pr != null) {
//            BufferedReader bfr = new BufferedReader(new InputStreamReader(pr.getInputStream()));
//            String line = "";
//            while((line = bfr.readLine()) != null)
//            {
//                if (line.contains("lect_data")) {
//                    logger.info("FOUND lect_data: " + line);
//                    temp = line;
//                } else {
//                    logger.info("" + line);
//                }
//            }
//        }
        logger.info("Tokens map: " + temp);

        if (temp.length() > 0) {
            if (temp.split("lect_data ").length > 1) {
                String array = temp.split("lect_data ")[1].replace("[", "").replace("]", "");
                logger.info("temp split length: " + temp.split("lect_data ").length);
                logger.info("array: " + array);
                logger.info("------");
                String[] a = array.split("<Lecture: [0-9]+>");
                logger.info("a: " + Arrays.toString(a));
                logger.info("a len: " + a.length);
                for (String lectureTokens : a) {
                    if (lectureTokens.contains("{")) {
                        logger.info("lectureTokens: " + lectureTokens);
                        String tokensMap = lectureTokens.split(", \\{")[1].replace("}", "");
                        logger.info("map: " + tokensMap);
                        String[] tokensArray = tokensMap.split(",");
                        for (String tokenPair : tokensArray) {
                            // logger.info("tokenPair: " + tokenPair);
                            if (tokenPair.trim().length() > 1) {
                                String count = tokenPair.split(":")[1];
                                String token = tokenPair.split(":")[0].replace("u'","").replace("'", "");
                                // logger.info("token: " + forceUtf8Coding(token) + ", count: " + count);
                                result.put(Integer.valueOf(removeNonDigits(count.trim())), token);
                                tokens.add(forceUtf8Coding(token.trim()));
                            }
                        }
                    }
                }
            }
        }

        if (tokens.size() > 0) {
            logger.info("Total tokens: "+ tokens.size());
            logger.info("Creating file to keep tokens");
            fileHandleService.createFile(course);

            logger.info("Writing tokens into file");
            String joined = String.join(",", tokens); // "foo and bar and baz"
            fileHandleService.writeIntoFile(course, joined);
        } else {
            logger.info("Not tokens collected.");
        }

        logger.info("Finished");
        return tokens;

    }

    public static String removeNonDigits(final String str) {
        if (str == null || str.length() == 0) {
            return "";
        }
        return str.replaceAll("\\D+", "");
    }

    private String forceUtf8Coding(String input) {
        // logger.info("forceUtf8Coding: " + input);
        // String converted = StringEscapeUtils.unescapeJava(input);
        return input;
    }

    private String getCurrentSemester() {
        int year = Calendar.getInstance().get(Calendar.YEAR);
        int month = Calendar.getInstance().get(Calendar.MONTH) + 1;
        String sem = "";
        if (month > 1 && month < 9) {
            sem = "S";
        } else {
            sem = "F";
        }
        return String.valueOf(year) + sem;
    }

    private void readProcessOutput(Process pr) throws IOException {
        logger.info("readProcessOutput");
        if (pr != null) {
            BufferedReader bfr = new BufferedReader(new InputStreamReader(pr.getInputStream()));
            String line = "";
            logger.info("lines: ");
            while((line = bfr.readLine()) != null)
            {
                logger.info("" + line);
            }
        }
    }

    private String  executeProcess(Process pr, Runtime rt, String[] cmds, boolean alltokens) {
        String result = "";
        try {
            pr = rt.exec(cmds);
            logger.info("waiting");
//            InputStream stderr = pr.getErrorStream();
//            InputStreamReader isr = new InputStreamReader(stderr);
//            BufferedReader br = new BufferedReader(isr);
//            String line = null;
//            logger.info("<ERROR>");
//            while ( (line = br.readLine()) != null)
//                logger.info(line);
//            logger.info("</ERROR>");
//            InputStream stdin = pr.getInputStream();
//            isr = new InputStreamReader(stdin);
//            br = new BufferedReader(isr);
//            line = null;
//            logger.info("<OUTPUT>");
//            while ( (line = br.readLine()) != null)
//                logger.info(line);
//            logger.info("</OUTPUT>");
            // any error message?
            StreamGobbler errorGobbler = new
                    StreamGobbler(pr.getErrorStream(), "LOG");

            // any output?
            StreamGobbler outputGobbler = new
                    StreamGobbler(pr.getInputStream(), "OUT");

            // kick them off
            errorGobbler.start();
            outputGobbler.start();
            int exitVal = pr.waitFor();
            if (alltokens) {
                result = outputGobbler.getCourse_data();
            } else {
                result = outputGobbler.getResult();
            }
            logger.info("exitVal: " + exitVal);
        } catch (IOException e) {
            logger.error("" + e.getMessage());;
        } catch (InterruptedException e) {
            logger.error("" + e.getMessage());
        }
        return result;
    }

}

class StreamGobbler extends Thread
{

    // https://stackoverflow.com/questions/20566267/streamgobbler-thread-to-accept-input-into-it-java

    private static final Logger logger = LoggerFactory.getLogger(StreamGobbler.class);

    InputStream is;
    String type;
    String result = "";
    String course_data = "";

    StreamGobbler(InputStream is, String type)
    {
        this.is = is;
        this.type = type;
    }

    public InputStream getIs() {
        return is;
    }

    public String getType() {
        return type;
    }

    public String getResult() {
        return result;
    }

    public String getCourse_data() {
        return course_data;
    }

    public void run()
    {
        try
        {
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line = "";
            while ( (line = br.readLine()) != null) {
                logger.info(type + " > " + line);
                if (line != null && line.contains("lect_data")) {
                    logger.info("FOUND lect_data: " + line);
                    result = line;
                } else if (line != null && line.contains("course_data_tokens")) {
                    logger.info("FOUND course_data_tokens: " + line);
                    course_data = line;
                } else {
                    if (line != null && line.contains("})]")) {
                        result = result + line;
                    }
                    // logger.info("" + line);
                }
            }

        } catch (IOException ioe)
        {
            ioe.printStackTrace();
        }
    }

}
