package ee.ssd.api.service;

import ee.ssd.api.data.entity.Course;
import ee.ssd.api.service.material.WebScraperService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("CourseService")
public class CourseService extends AbstractService {

    @Autowired
    private WebScraperService webScraperService;

    public List<Course> findAll() {

        return webScraperService.getAllCourses();
    }
}
