package ee.ssd.api.service;

import ee.ssd.api.data.dto.request.material.MaterialListGetRequest;
import ee.ssd.api.data.entity.Material;
import ee.ssd.api.service.material.RecommendArticleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service("MaterialService")
public class MaterialService extends AbstractService  {

    @Autowired
    RecommendArticleService recommendArticleService;

    public List<Material> find(MaterialListGetRequest request) throws IOException {
        List<Material> list = recommendArticleService.getRecommendedArticlesForCourse(request.code);
        return list;
    }

}
