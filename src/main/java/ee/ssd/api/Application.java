package ee.ssd.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.math.RoundingMode;
import java.util.Objects;
import java.util.TimeZone;

@SpringBootApplication
@EnableScheduling
public class Application extends SpringBootServletInitializer {
    public static final String VERSION = "0.0.0";
    public static final RoundingMode ROUNDING = RoundingMode.HALF_UP;

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }

    public static void main(String[] args) {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));

        SpringApplication.run(Application.class, args);
    }

    /**
     * @return true when and only when environment variable PRODUCTION is to to "true"
     */
    public static boolean isProduction() {
        String name = "PRODUCTION";

        return System.getenv().containsKey(name) && Objects.equals(System.getenv(name).toLowerCase(), "true");
    }
}
