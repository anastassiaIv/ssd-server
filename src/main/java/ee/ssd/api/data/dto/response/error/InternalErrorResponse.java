package ee.ssd.api.data.dto.response.error;

import ee.ssd.api.controller.exception.ErrorCodeEnum;

public class InternalErrorResponse extends ErrorResponse {
    public final String id;

    public InternalErrorResponse(String id) {
        super(ErrorCodeEnum.INTERNAL_ERROR);
        this.id = id;
    }
}