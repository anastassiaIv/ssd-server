package ee.ssd.api.data.dto.request.material;

public class MaterialListGetRequest {

    public String code;

    public MaterialListGetRequest(String code) {
        this.code = code;
    }
}
