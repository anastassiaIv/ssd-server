package ee.ssd.api.data.dto.response.course;

import ee.ssd.api.data.entity.Course;

public class CourseResponse {

    public String code;
    public String name;
    public String url;

    public static CourseResponse convertFromEntity(Course entity) {

        CourseResponse response = new CourseResponse();
        response.code = entity.getCode();
        response.name = entity.getName();
        response.url = entity.getUrl();
        return response;
    }
}