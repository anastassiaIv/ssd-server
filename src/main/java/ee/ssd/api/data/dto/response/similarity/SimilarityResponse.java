package ee.ssd.api.data.dto.response.similarity;

import ee.ssd.api.data.entity.Similarity;

public class SimilarityResponse {

    public String course;
    public double similarity;

    public static SimilarityResponse convertFromEntity(Similarity entity) {

        SimilarityResponse response = new SimilarityResponse();
        response.course = entity.getCourse();
        response.similarity = entity.getSimilarity();
        return response;
    }
}
