package ee.ssd.api.data.dto.response.error;

import ee.ssd.api.controller.exception.ValidationErrorCodeEnum;

public class ValidationErrorRow {
    private final String field;
    private final ValidationErrorCodeEnum code;
    private final String explanation;

    // todo: make private and add convenience methods
    public ValidationErrorRow(String field, ValidationErrorCodeEnum code) {
        this.field = field;
        this.code = code;
        this.explanation = null;
    }

    public String getField() {
        return field;
    }

    public ValidationErrorCodeEnum getCode() {
        return code;
    }

    public String getExplanation() {
        return explanation;
    }

    public static ValidationErrorRow required(String field) {
        return new ValidationErrorRow(field, ValidationErrorCodeEnum.REQUIRED);
    }

    public static ValidationErrorRow invalid(String field) {
        return new ValidationErrorRow(field, ValidationErrorCodeEnum.INVALID);
    }

    public static ValidationErrorRow forbidden(String field) {
        return new ValidationErrorRow(field, ValidationErrorCodeEnum.FORBIDDEN);
    }
}
