package ee.ssd.api.data.dto.response.error;

import ee.ssd.api.controller.exception.ErrorCodeEnum;

public class ErrorResponse {

    public final ErrorCodeEnum code;

    public ErrorResponse(ErrorCodeEnum code) {
        this.code = code;
    }
}
