package ee.ssd.api.data.dto.response.error;

import ee.ssd.api.controller.exception.ErrorCodeEnum;
import ee.ssd.api.controller.exception.ValidationException;

import java.util.HashSet;
import java.util.Set;

public class ValidationErrorResponse extends ErrorResponse {
    public final Set<ValidationErrorRow> rows;
    public final String message;

    public ValidationErrorResponse(ErrorCodeEnum code) {
        super(code);
        this.rows = new HashSet<>();
        this.message = null;
    }

    public ValidationErrorResponse(ValidationException e) {
        super(e.getCode());
        this.rows = e.getRows();
        this.message = e.getMessage();
    }

    public ValidationErrorResponse(ValidationErrorRow row) {
        super(ErrorCodeEnum.VALIDATION_ERROR);

        rows = new HashSet<>();
        rows.add(row);
        this.message = null;
    }
}