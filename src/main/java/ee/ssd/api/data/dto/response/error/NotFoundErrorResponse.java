package ee.ssd.api.data.dto.response.error;

import ee.ssd.api.controller.exception.EntityEnum;
import ee.ssd.api.controller.exception.NotFoundException;

public class NotFoundErrorResponse extends ErrorResponse {
    public final EntityEnum entity;
    public final String field;
    public final String id;

    public NotFoundErrorResponse(NotFoundException e) {
        super(e.getCode());
        this.entity = e.getEntity();
        this.field = e.getField();
        this.id = e.getId();
    }
}
