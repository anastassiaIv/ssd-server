package ee.ssd.api.data.dto.response.material;

import ee.ssd.api.data.entity.Material;

public class MaterialResponse {

    public String title;
    public String link;

    public static MaterialResponse convertFromEntity(Material entity) {
        MaterialResponse response = new MaterialResponse();

        response.title = entity.getTitle();
        response.link = entity.getLink();

        return response;
    }
}
