package ee.ssd.api.data.entity;


/**
 * Created by anastassia on 06/07/2017.
 */

public class Material {

    private String title;
    private String link;

    public Material() {}

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
