package ee.ssd.api.exception;

public class AccessRestrictedException extends RuntimeException {

    /**
     * Creation must be done via methods for specific entities
     *
     * @param message Description of access violation. This message must be logged at some point
     */
    private AccessRestrictedException(String message) {
        super(message);
    }

}

