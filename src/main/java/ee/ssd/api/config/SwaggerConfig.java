package ee.ssd.api.config;

import ee.ssd.api.Application;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.*;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket apiDoc() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Internal API")
                .select()
                .apis(RequestHandlerSelectors.basePackage("ee.ssd.api.controller"))
                .paths(PathSelectors.any())
                .build()
                .globalResponseMessage(RequestMethod.GET, responseMessages())
                .globalResponseMessage(RequestMethod.POST, responseMessages())
                .globalResponseMessage(RequestMethod.PUT, responseMessages())
                .globalResponseMessage(RequestMethod.PATCH, responseMessages())
                .globalResponseMessage(RequestMethod.DELETE, responseMessages())
                .apiInfo(internalInfo())
                .useDefaultResponseMessages(false);
    }

    private ApiInfo internalInfo() {
        return new ApiInfo(
                "Internal API",
                "Internal API documentation\n\n**Please note that pages are 0-indexed**",
                Application.VERSION,
                null,
                contact(),
                null,
                null,
                new ArrayList<>()
        );
    }

    private List<ResponseMessage> responseMessages() {
        List<ResponseMessage> messages = new ArrayList<>();

        messages.add(new ResponseMessageBuilder()
                .code(HttpServletResponse.SC_UNAUTHORIZED)
                .message("Authentication required")
                .responseModel(new ModelRef("ErrorResponse"))
                .build());

        messages.add(new ResponseMessageBuilder()
                .code(HttpServletResponse.SC_FORBIDDEN)
                .message("You do not have access to this resource")
                .responseModel(new ModelRef("ErrorResponse"))
                .build());

        messages.add(new ResponseMessageBuilder()
                .code(HttpServletResponse.SC_INTERNAL_SERVER_ERROR)
                .message("Internal error")
                .responseModel(new ModelRef("InternalErrorResponse"))
                .build());


        return messages;
    }

    private Contact contact() {
        return new Contact("Anastassia Ivanova", "", "anastassia.ivanova@ut.ee");
    }

    @Bean
    public SecurityConfiguration securityConfiguration() {
        return SecurityConfigurationBuilder.builder().build();
    }

    @Bean
    public UiConfiguration uiConfig() {
        return UiConfigurationBuilder
                .builder()
                .docExpansion(DocExpansion.LIST)
                .defaultModelRendering(ModelRendering.EXAMPLE)
                .filter(true)
                .build();
    }
}
