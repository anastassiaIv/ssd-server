FROM bitweb/gradle:4.8-java10

COPY . /data
WORKDIR /data

# Downloads dependencies, compiles code and builds .jar file. Will not run tests.
RUN gradle bootJar && mv build/libs/api-*.jar api.jar

CMD ["java", "-Dfile.encoding=UTF-8", "-jar", "api.jar"]